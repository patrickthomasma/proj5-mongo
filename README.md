# Project 5 brev time mongo DB
This is my project in which I implement a display window/database unto the project 4 project
# Style
In my implementation I decided to do alot of the legwork inside of calc.html for my database and when it came to the flask implementation I just tracked through what I've already updated and added then opened a display window which would grab the information I had added onto submit and then just display how I thought would look good to display

# How to use
There is a script in the program called run.sh, all you have to do is type into the terminal ./run.sh and the webpage will be up! (Make sure you enable proper permissions on the script ex: chmod 777 run.sh) if the user puts in a incorrect mileage/km then the program will return the current date and time to symbolize that is an improper way of putting in the data, if user doesn't submit any data display page will be blank

# Testing
In the Dockerfile there is a step to run the testing file and if downloaded right the tests should come out      a-okay!!!
 
# User information
Written by Patrick Thomasma
Email: pthomasm@uoregon.edu
